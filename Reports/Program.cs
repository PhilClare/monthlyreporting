﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http.Headers;
using System.Net.Http.Extensions.Compression.Client;
using System.Net.Http.Extensions.Compression.Core.Compressors;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

using Aspose.Cells;
using Newtonsoft.Json;
using Pod.Telemetry.Reporting.Models;
//using Pod.Core.Models;
using Simple.OData.Client;
using System.Net.Mime;
using Aspose.Cells.Rendering;

namespace Reports
{
	class Program
	{
		static void Main(string[] args)
		{
			MainAsync().GetAwaiter().GetResult();
		}

		static async Task MainAsync()
		{
			//await GenerateTelemetryReport();
			await ManagementReport.GenerateManagementReport();
		}

		static async Task GenerateTelemetryReport()
		{
			//DateTime end = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
			//DateTime start = end.AddMonths(-1);
			DateTime end = DateTime.Now;
			DateTime start = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);

			var tenantSubscriptions = await GetTenants();
			//var tenantSubscriptions = new[]
			//{
			//	new
			//	{
			//		ReportingEnabled = true,
			//		TenantGuid = new Guid("9419c181-f364-4020-8c9e-8fef15fbf6c5"),
			//		TenantName = "WorkWave",
			//		BccOwnerOnEmails = false,
			//		OwnerEmailAddress = "",
			//		Subscribers = new[]
			//		{
			//			new {SubscriberEmailAddress = "phil.clare@interact-intranet.com"}
			//		}
			//	}
			//};

			foreach (var subscription in tenantSubscriptions)
			{
				if (subscription.ReportingEnabled && subscription.Subscribers != null && subscription.Subscribers.Count > 0)
				{
					var tenantId = subscription.TenantGuid;
					var tenantName = subscription.TenantName;

					try
					{
						// Produce Report
						Workbook report = await ProduceReport(tenantId, tenantName, start, end);

						var file = Path.Combine(Settings.Default.TemplatePath, $"{tenantName} - Telemetry.pdf");

						var options = new PdfSaveOptions()
						{
							PdfCompression = PdfCompressionCore.None,
							EmbedStandardWindowsFonts = true,
							PrintingPageType = PrintingPageType.IgnoreBlank,
							RefreshChartCache = true,
							CalculateFormula = true
						};

						report.Save(file, SaveFormat.Pdf);
						report.Save(Path.Combine(Settings.Default.TemplatePath, $"{tenantName} - Telemetry.xlsx"), SaveFormat.Xlsx);

						// send mail
						MailMessage message = new MailMessage();
						message.From = new MailAddress(Settings.Default.smtpfromaddress);

						foreach (var sub in subscription.Subscribers)
						{
							if (sub.SubscriberEmailAddress != null && sub.SubscriberEmailAddress != string.Empty)
							{
								message.To.Add(new MailAddress(sub.SubscriberEmailAddress));
							}
						}

						if (subscription.BccOwnerOnEmails && subscription.OwnerEmailAddress != string.Empty)
						{
							message.Bcc.Add(new MailAddress(subscription.OwnerEmailAddress));
						};

						message.Subject = $"Intranet engagement report for {tenantName} - {start.ToString("dd MMMM yyyy")}";
						message.Body = string.Empty;
						message.IsBodyHtml = true;

						// Create the file attachment for this e-mail message.
						Attachment data = new Attachment(file, MediaTypeNames.Application.Octet);

						// Add time stamp information for the file.
						System.Net.Mime.ContentDisposition disposition = data.ContentDisposition;
						disposition.CreationDate = System.IO.File.GetCreationTime(file);
						disposition.ModificationDate = System.IO.File.GetLastWriteTime(file);
						disposition.ReadDate = System.IO.File.GetLastAccessTime(file);

						// Add the file attachment to this e-mail message.
						message.Attachments.Add(data);

						using (SmtpClient client = new SmtpClient())
						{
							client.Host = Settings.Default.smtpserver;
							client.Port = Settings.Default.smtpport;
							client.EnableSsl = true;
							client.UseDefaultCredentials = false;
							client.DeliveryMethod = SmtpDeliveryMethod.Network;
							client.Credentials =
								new System.Net.NetworkCredential(Settings.Default.smtpusername, Settings.Default.smtppassword);

							client.Send(message);
						}

						break;
					}
					catch (Exception ex)
					{
						Program.Logger(ex.Message);
						Program.Logger(ex.StackTrace);
					}
				}
			}
		}

		static async Task<Workbook> ProduceReport(Guid tenantGuid, string tenantName, DateTime startDate, DateTime endDate)
		{
			var wb = new Aspose.Cells.Workbook(Path.Combine(Settings.Default.TemplatePath, "Telemetry.xlsx"));

			var cellsLicense = new Aspose.Cells.License();
			cellsLicense.SetLicense("Aspose.Total.lic");

			await Sources.Telemetry.GetTelemetry(tenantGuid, startDate, endDate, wb.Worksheets["data"], Settings.Default.telemetryUser, Settings.Default.telemetryPass, Settings.Default.reportsurl);
			//await Sources.Zendesk.GetServicedeskData(tenantName, startDate, endDate, wb.Worksheets["data"]);

			// load general info
			wb.Worksheets["data"].Cells[35, 4].PutValue(tenantName);
			wb.Worksheets["data"].Cells[34, 4].PutValue(startDate.ToString("MMMM yyyy"));

			wb.CalculateFormula(true);

			foreach (var ws in wb.Worksheets)
			{
				foreach (var cht in ws.Charts)
				{
					cht.RefreshPivotData();
					cht.Calculate();
					cht.RefreshPivotData();
				}
			}

			return wb;
		}

		static async Task<List<TenantSubscription>> GetTenants()
		{
			var sql = $@"select * from tenantsubscribers inner join tenantsubscriptions on tenantsubscribers.tenantguid = tenantsubscriptions.tenantguid";

			var result = new Dictionary<Guid, TenantSubscription>();

			using (var conn = new SqlConnection(Settings.Default.Control))
			{
				conn.Open();
				using (SqlCommand command = new SqlCommand(sql, conn))
				{
					using (var dr = command.ExecuteReader())
					{
						while (dr.Read())
						{
							// match each set of subscribers to their tenant
							var tenantGuid = new Guid(dr["tenantguid"].ToString());
							if (!result.ContainsKey(tenantGuid))
							{
								// add headline information
								var sub = new TenantSubscription()
								{
									BccOwnerOnEmails = (bool) dr["BCCOwnerOnEmails"],
									OwnerEmailAddress = (string) dr["OwnerEmail"],
									OwnerName = (string) dr["OwnerName"],
									ReportingEnabled = (bool) dr["ReportingEnabled"],
									Subscribers = new List<TenantSubscriber>(),
									TenantGuid = tenantGuid,
									TenantName = (string) dr["Tenantname"]
								};
								result.Add(tenantGuid, sub);
							}
							
							result[tenantGuid].Subscribers.Add(new TenantSubscriber()
							{
								SubscriberEmailAddress = (string)dr["SubscriberEmailaddress"],
								SubscriberName = (string)dr["SubscriberDisplayName"]
							});
						}
					}
				}
			}
			
			return result.Select(r => r.Value).ToList();
		}

		public static void Logger(String lines)
		{
			System.IO.StreamWriter file = new System.IO.StreamWriter(Path.Combine(DateTime.Now.ToString("yyyyMMdd") + ".log"), true);
			file.WriteLine($"{DateTime.Now} - {lines}");

			file.Close();
		}
	}
}
