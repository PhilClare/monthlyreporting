﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http.Headers;
using System.Net.Http.Extensions.Compression.Client;
using System.Net.Http.Extensions.Compression.Core.Compressors;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

using Aspose.Cells;
using Newtonsoft.Json;
using Pod.Telemetry.Reporting.Models;
//using Pod.Core.Models;
using Simple.OData.Client;
using System.Net.Mime;
using Aspose.Cells.Rendering;

namespace Reports
{
	class ManagementReport
	{
		public static async Task GenerateManagementReport()
		{
			var start = DateTime.Today.AddDays(-7);
			
			// Produce Report
			Workbook report = await ProduceReport(start);

			var file = Path.Combine(Settings.Default.TemplatePath, "HighLevelReport.xlsx");
			report.Save(file, SaveFormat.Xlsx);
			
			// send mail
			MailMessage message = new MailMessage();
			message.From = new MailAddress(Settings.Default.smtpfromaddress);

			// message.To.Add(new MailAddress("simon.dance@interact-intranet.com"));
			message.To.Add(new MailAddress("phil.clare@interact-intranet.com"));

			message.Subject = $"High level telemetry report - {start.ToString("dd MMMM yyyy")}";
			message.Body = string.Empty;
			message.IsBodyHtml = true;

			// Create the file attachment for this e-mail message.
			Attachment data = new Attachment(file, MediaTypeNames.Application.Octet);

			// Add time stamp information for the file.
			System.Net.Mime.ContentDisposition disposition = data.ContentDisposition;
			disposition.CreationDate = System.IO.File.GetCreationTime(file);
			disposition.ModificationDate = System.IO.File.GetLastWriteTime(file);
			disposition.ReadDate = System.IO.File.GetLastAccessTime(file);

			// Add the file attachment to this e-mail message.
			message.Attachments.Add(data);

			using (SmtpClient client = new SmtpClient())
			{
				client.Host = Settings.Default.smtpserver;
				client.Port = Settings.Default.smtpport;
				client.EnableSsl = true;
				client.UseDefaultCredentials = false;
				client.DeliveryMethod = SmtpDeliveryMethod.Network;
				client.Credentials =
					new System.Net.NetworkCredential(Settings.Default.smtpusername, Settings.Default.smtppassword);

				client.Send(message);
			}
		}

		static async Task<Workbook> ProduceReport(DateTime startDate)
		{
			var wb = new Aspose.Cells.Workbook(Path.Combine(Settings.Default.TemplatePath, "HighLevelTemplate.xlsx"));

			var cellsLicense = new Aspose.Cells.License();
			cellsLicense.SetLicense("Aspose.Total.lic");

			await Sources.HighLevelTelemetry.GetHighLevelTelemetry(startDate, Settings.Default.telemetryUser, Settings.Default.telemetryPass, Settings.Default.reportsurl, wb);
			
			wb.CalculateFormula(true);
			
			return wb;
		}
	}
}
