﻿using Aspose.Cells;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Pod.Telemetry.Reporting.Models;
using Pod.Core.Models;
using Simple.OData.Client;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Http.Extensions.Compression.Client;
using System.Net.Http.Extensions.Compression.Core.Compressors;

namespace Reports.Sources
{
	class HighLevelTelemetry
	{
		public static async Task GetHighLevelTelemetry(DateTime startDate, string username, string password,
			string reportsUrl, Workbook wb)
		{
			var start = (DateTimeOffset?)startDate;
			var end = (DateTimeOffset?)startDate.AddDays(2).AddSeconds(-1);

			// Create client with given base URI and credentials and enable gzip
			var settings = new ODataClientSettings
			{
				BaseUri = new Uri($"{reportsUrl}/odata"),
				OnCreateMessageHandler =
					() =>
						new ClientCompressionHandler(new GZipCompressor()),
				BeforeRequest = request =>
				{
					request.Headers.Add("Accept-Encoding", "gzip");
					request.Headers.Authorization = new AuthenticationHeaderValue(
						"Basic",
						Convert.ToBase64String(
							Encoding.ASCII.GetBytes(
								$"{username}:{password}")));
				}
			};

			var client = new ODataClient(settings);

			// Get Tenant Details
			var tenantsWithLicense = GetTenantsWithLicense();
			var tenants = GetTenants();

			// Get Aggregate
			await GetAggregate(client, start, end, wb, tenantsWithLicense);

			// Get Modules
			await GetModules(client, startDate, wb, tenants);

			// Get JSON
			await GetJson(client, startDate, end, wb, tenantsWithLicense);
		}

		private static async Task GetAggregate(ODataClient client, DateTimeOffset? start, DateTimeOffset? end, Workbook wb, Dictionary<string, string> tenants)
		{
			var processedKeys = new List<string>();
			var annotations = new ODataFeedAnnotations();

			var usage = (await client
					.For<AggregateDataResponse>()
					.Filter(p => p.InsertionDate >= start &&
						p.InsertionDate <= end)
					.FindEntriesAsync(annotations))
				.ToList();

			while (annotations.NextPageLink != null)
			{
				usage.AddRange(await client
					.For<AggregateDataResponse>()
					.Filter(p => p.InsertionDate >= start &&
						p.InsertionDate <= end)
					.FindEntriesAsync(annotations.NextPageLink, annotations));
			}

			var ws = wb.Worksheets["Usage"];
			var i = 1;
			foreach (var agg in usage)
			{
				var key = (agg.PayLoad.TenantGuid + "_" + agg.PayLoad.LicenceKey).ToUpper();
				var tenant = "Unknown";

				if (!processedKeys.Contains(key))
				{
					if (tenants.ContainsKey(key))
					{
						tenant = tenants[key];
					}

					ws.Cells[i, 0].PutValue(tenant);
					ws.Cells[i, 1].PutValue(agg.PayLoad.LicencesPermitted);
					ws.Cells[i, 2].PutValue(agg.PayLoad.ActiveUserCount);
					ws.Cells[i, 3].PutValue(agg.PayLoad.MonthlyActiveUsersCurr);
					ws.Cells[i, 4].PutValue(agg.PayLoad.MonthlyActiveUsersPastFirstPageCurr);
					ws.Cells[i, 5].PutValue(agg.PayLoad.MonthlyActiveUsersPrev);
					ws.Cells[i, 6].PutValue(agg.PayLoad.MonthlyActiveUsersPastFirstPagePrev);

					ws.Cells[i, 13].PutValue(agg.PayLoad.TenantGuid);
					ws.Cells[i, 14].PutValue(agg.PayLoad.LicenceKey);

					i++;

					processedKeys.Add(key);
				}
			}

			//DataSorter sorter = wb.DataSorter;
			//sorter.Order1 = Aspose.Cells.SortOrder.Ascending;
			//sorter.Key1 = 0;
			//CellArea ca = new CellArea();
			//ca.StartRow = 1;
			//ca.StartColumn = 0;
			//ca.EndRow = 300;
			//ca.EndColumn = 15;
			//sorter.Sort(ws.Cells, ca);

		}

		private static async Task GetModules(ODataClient client, DateTime startDate,
			Workbook wb, Dictionary<Guid, string> tenants)
		{
			var mods = new Dictionary<Guid, modData>();
			var endDate = startDate.AddDays(1).AddSeconds(-1);
			startDate = startDate.AddDays(-30);
			var annotations = new ODataFeedAnnotations();

			var usage = (await client
					.For<Page>()
					.Filter(p => p.CreatedDate >= (DateTimeOffset?)startDate &&
					             p.CreatedDate <= (DateTimeOffset?)endDate)
					.FindEntriesAsync(annotations))
				.ToList();

			while (annotations.NextPageLink != null)
			{
				usage.AddRange(await client
					.For<Page>()
					.Filter(p => p.CreatedDate >= (DateTimeOffset?)startDate &&
					             p.CreatedDate <= (DateTimeOffset?)endDate)
					.FindEntriesAsync(annotations.NextPageLink, annotations));
			}

			foreach (var use in usage)
			{
				var tg = use.TenantGuid.Value;
				var mod = new modData();

				if (!mods.ContainsKey(tg))
				{
					mods.Add(tg, mod);	
				}

				switch (use.Module)
				{
					case "Absence Booking":
						mods[tg].AbsenceBooking += use.Total.Value;
						break;
					case "Administration":
						mods[tg].Administration += use.Total.Value;
						break;
					case "Analytics":
						mods[tg].Analytics += use.Total.Value;
						break;
					case "Blog":
						mods[tg].Blog += use.Total.Value;
						break;
					case "Business Contacts":
						mods[tg].BusinessContacts += use.Total.Value;
						break;
					case "Calendar":
						mods[tg].Calendar += use.Total.Value;
						break;
				    case "Case Management":
				        mods[tg].CaseManagement += use.Total.Value;
				        break;
					case "Category":
						mods[tg].Category += use.Total.Value;
						break;
					case "Content Area / Team Homepage / Homepage":
						mods[tg].ContentAreaTeamHomepageHomepage += use.Total.Value;
						break;
					case "Error Page":
						mods[tg].ErrorPage += use.Total.Value;
						break;
					case "Expenses":
						mods[tg].Expenses += use.Total.Value;
						break;
					case "Football":
						mods[tg].Football += use.Total.Value;
						break;
					case "Forums":
						mods[tg].Forums += use.Total.Value;
						break;
					case "Gallery":
						mods[tg].Gallery += use.Total.Value;
						break;
                    case "Getting Started":
                        mods[tg].GettingStarted += use.Total.Value;
                        break;
                    case "Homepage":
                        mods[tg].HomePage += use.Total.Value;
                        break;
				    case "Manage Homepages Admin":
				        mods[tg].ManagerHomepagesAdmin += use.Total.Value;
				        break;
				    case "Manage IP Addresses":
				        mods[tg].ManageIpAddresses += use.Total.Value;
				        break;
                    case "Network Directory":
						mods[tg].NetworkDirectory += use.Total.Value;
						break;
					case "Notifications":
						mods[tg].Notifications += use.Total.Value;
						break;
					case "Organogram":
						mods[tg].Organogram += use.Total.Value;
						break;
					case "Other":
						mods[tg].Other += use.Total.Value;
						break;
					case "Page":
						mods[tg].Page += use.Total.Value;
						break;
					case "Page Editor":
						mods[tg].PageEditor += use.Total.Value;
						break;
					case "People Directory":
						mods[tg].PeopleDirectory += use.Total.Value;
						break;
				    case "Polls":
				        mods[tg].Polls += use.Total.Value;
				        break;
                    case "Room & Resource":
						mods[tg].RoomResource += use.Total.Value;
						break;
					case "Search":
						mods[tg].Search += use.Total.Value;
						break;
				    case "SharePoint Admin":
				        mods[tg].SharepointAdmin += use.Total.Value;
				        break;
				    case "Short URL Admin":
				        mods[tg].ShortUrlAdmin += use.Total.Value;
				        break;
				    case "System Text Admin":
				        mods[tg].SystemTextAdmin += use.Total.Value;
				        break;
                    case "Tasks":
						mods[tg].Tasks += use.Total.Value;
						break;
					case "Team Blog":
						mods[tg].TeamBlog += use.Total.Value;
						break;
					case "Teams":
						mods[tg].Teams += use.Total.Value;
						break;
				    case "Training Manager":
				        mods[tg].TrainingManager += use.Total.Value;
				        break;
				    case "UMI":
				        mods[tg].Umi += use.Total.Value;
				        break;
                    case "Workflow & Forms":
						mods[tg].WorkflowForms += use.Total.Value;
						break;
				}
			}

			var ws = wb.Worksheets["Modules"];
			var processedKeys = new List<Guid>();

			var i = 1;
			foreach (var mod in mods)
			{
				var key = mod.Key;
				var tenant = "Unknown";

				if (!processedKeys.Contains(key))
				{
					if (tenants.ContainsKey(key))
					{
						tenant = tenants[key];
					}

					ws.Cells[i, 0].PutValue(tenant);
					ws.Cells[i, 1].PutValue(mod.Value.AbsenceBooking);
					ws.Cells[i, 2].PutValue(mod.Value.Administration);
					ws.Cells[i, 3].PutValue(mod.Value.Analytics);
					ws.Cells[i, 4].PutValue(mod.Value.Blog);
					ws.Cells[i, 5].PutValue(mod.Value.BusinessContacts);
					ws.Cells[i, 6].PutValue(mod.Value.Calendar);
				    ws.Cells[i, 7].PutValue(mod.Value.CaseManagement);
                    ws.Cells[i, 8].PutValue(mod.Value.Category);
				    ws.Cells[i, 9].PutValue(mod.Value.ContentAreaTeamHomepageHomepage);
					ws.Cells[i, 10].PutValue(mod.Value.ErrorPage);
					ws.Cells[i, 11].PutValue(mod.Value.Expenses);
					ws.Cells[i, 12].PutValue(mod.Value.Football);
					ws.Cells[i, 13].PutValue(mod.Value.Forums);
					ws.Cells[i, 14].PutValue(mod.Value.Gallery);
				    ws.Cells[i, 15].PutValue(mod.Value.GettingStarted);
				    ws.Cells[i, 16].PutValue(mod.Value.HomePage);
				    ws.Cells[i, 17].PutValue(mod.Value.ManagerHomepagesAdmin);
				    ws.Cells[i, 18].PutValue(mod.Value.ManageIpAddresses);
                    ws.Cells[i, 19].PutValue(mod.Value.NetworkDirectory);
					ws.Cells[i, 20].PutValue(mod.Value.Notifications);
					ws.Cells[i, 21].PutValue(mod.Value.Organogram);
					ws.Cells[i, 22].PutValue(mod.Value.Other);
					ws.Cells[i, 23].PutValue(mod.Value.Page);
					ws.Cells[i, 24].PutValue(mod.Value.PageEditor);
					ws.Cells[i, 25].PutValue(mod.Value.PeopleDirectory);
				    ws.Cells[i, 26].PutValue(mod.Value.Polls);
                    ws.Cells[i, 27].PutValue(mod.Value.RoomResource);
					ws.Cells[i, 28].PutValue(mod.Value.Search);
				    ws.Cells[i, 29].PutValue(mod.Value.SharepointAdmin);
				    ws.Cells[i, 30].PutValue(mod.Value.ShortUrlAdmin);
				    ws.Cells[i, 31].PutValue(mod.Value.SystemTextAdmin);
                    ws.Cells[i, 32].PutValue(mod.Value.Tasks);
					ws.Cells[i, 33].PutValue(mod.Value.TeamBlog);
					ws.Cells[i, 34].PutValue(mod.Value.Teams);
				    ws.Cells[i, 35].PutValue(mod.Value.TrainingManager);
                    ws.Cells[i, 36].PutValue(mod.Value.Umi);
                    ws.Cells[i, 37].PutValue(mod.Value.WorkflowForms);

					i++;

					processedKeys.Add(key);
				}
			}

			//DataSorter sorter = wb.DataSorter;
			//sorter.Order1 = Aspose.Cells.SortOrder.Ascending;
			//sorter.Key1 = 0;
			//CellArea ca = new CellArea();
			//ca.StartRow = 1;
			//ca.StartColumn = 0;
			//ca.EndRow = 600;
			//ca.EndColumn = 30;
			//sorter.Sort(ws.Cells, ca);
		}

		private static Dictionary<Guid, string> GetTenants()
		{
			var sql = $@"select domain, tenantguid 'key' from mastercustomers 
				inner join mastertenants on mastercustomers.id = mastertenants.customerid 
				inner join masterlicenses on masterlicenses.id = mastertenants.id";

			var result = new Dictionary<Guid, string>();

			using (var conn = new SqlConnection(Settings.Default.Control))
			{
				conn.Open();
				using (SqlCommand command = new SqlCommand(sql, conn))
				{
					using (var dr = command.ExecuteReader())
					{
						while (dr.Read())
						{
							// match each set of subscribers to their tenant
							var dom = (string)dr["domain"];
							var key = (Guid)dr["key"];
							if (!result.ContainsKey(key))
							{
								result.Add(key, dom);
							}
						}
					}
				}
			}

			return result;
		}

		private static Dictionary<string, string> GetTenantsWithLicense()
		{
			var sql = $@"select domain, concat(tenantguid,'_',licensekey) 'key' from mastercustomers 
				inner join mastertenants on mastercustomers.id = mastertenants.customerid 
				inner join masterlicenses on masterlicenses.id = mastertenants.id";

			var result = new Dictionary<string, string>();

			using (var conn = new SqlConnection(Settings.Default.Control))
			{
				conn.Open();
				using (SqlCommand command = new SqlCommand(sql, conn))
				{
					using (var dr = command.ExecuteReader())
					{
						while (dr.Read())
						{
							// match each set of subscribers to their tenant
							var dom = (string)dr["domain"];
							var key = ((string)dr["key"]).ToUpper();
							if (!result.ContainsKey(key))
							{
								result.Add(key, dom);
							}
						}
					}
				}
			}

			return result;
		}

		private static async Task GetJson(ODataClient client, DateTimeOffset? start, DateTimeOffset? end, Workbook wb, Dictionary<string, string> tenants)
		{
			var processedKeys = new List<string>();
			var annotations = new ODataFeedAnnotations();

			var usage = (await client
					.For<AggregateDataResponse>()
					.Filter(p => p.InsertionDate >= start &&
					             p.InsertionDate <= end)
					.FindEntriesAsync(annotations))
				.ToList();

			while (annotations.NextPageLink != null)
			{
				usage.AddRange(await client
					.For<AggregateDataResponse>()
					.Filter(p => p.InsertionDate >= start &&
								 p.InsertionDate <= end)
					.FindEntriesAsync(annotations.NextPageLink, annotations));
			}

			var ws = wb.Worksheets["JSON"];

			// Add Header row
			var i = 0;
			ws.Cells[i, 0].PutValue("tenant");
			ws.Cells[i, 1].PutValue("TenantGuid");
			ws.Cells[i, 2].PutValue("LicenceKey");
			ws.Cells[i, 3].PutValue("LicencesPermitted");
			ws.Cells[i, 4].PutValue("ActiveUserCount");
			ws.Cells[i, 5].PutValue("InactiveUserCount");
			ws.Cells[i, 6].PutValue("ContentAreaCount");
			ws.Cells[i, 7].PutValue("BlogCount");
			ws.Cells[i, 8].PutValue("TeamCount");
			ws.Cells[i, 9].PutValue("HighWarningCount");
			ws.Cells[i, 10].PutValue("MediumWarningCount");
			ws.Cells[i, 11].PutValue("LowWarningCount");
			ws.Cells[i, 12].PutValue("PagesAdded");
			ws.Cells[i, 13].PutValue("BlogPostsAdded");
			ws.Cells[i, 14].PutValue("ForumThreadsAdded");
			ws.Cells[i, 15].PutValue("ForumPostsAdded");
			ws.Cells[i, 16].PutValue("TimelineEntriesAdded");
			ws.Cells[i, 17].PutValue("EndorsementsAdded");
			ws.Cells[i, 18].PutValue("OptIn");
			ws.Cells[i, 19].PutValue("Sector");
			ws.Cells[i, 20].PutValue("UserRange");
			ws.Cells[i, 21].PutValue("NumberOfContributors");
			ws.Cells[i, 22].PutValue("NumberOfExpiredPages");
			ws.Cells[i, 23].PutValue("NumberOfUnrenewedPages");
			ws.Cells[i, 24].PutValue("MonthlyActiveUsersPrev");
			ws.Cells[i, 25].PutValue("MonthlyActiveVisitsPrev");
			ws.Cells[i, 26].PutValue("MonthlyActiveUsersPastFirstPagePrev");
			ws.Cells[i, 27].PutValue("MonthlyVisitsUniquePastFirstPagePrev");
			ws.Cells[i, 28].PutValue("MonthlyActiveUsersCurr");
			ws.Cells[i, 29].PutValue("MonthlyActiveVisitsCurr");
			ws.Cells[i, 30].PutValue("MonthlyActiveUsersPastFirstPageCurr");
			ws.Cells[i, 31].PutValue("MonthlyVisitsUniquePastFirstPageCurr");
			ws.Cells[i, 32].PutValue("OptInLastUpdatedBy");
			ws.Cells[i, 33].PutValue("OptInlastUpdatedDate");
			ws.Cells[i, 34].PutValue("Version");
			ws.Cells[i, 35].PutValue("DomainName");

			var offset = 36;
			var count = 0;

			for (var c = 0; c < 10; c++)
			{
				ws.Cells[i, offset + count].PutValue($"ActiveDepartment {c} - ItemId");
				ws.Cells[i, offset + count + 1].PutValue($"ActiveDepartment {c} - ItemCount");
				ws.Cells[i, offset + count + 2].PutValue($"ActiveDepartment {c} - ItemText");

				count += 3;
			}

			offset += 30;
			count = 0;

			for (var c = 0; c < 10; c++)
			{
				ws.Cells[i, offset + count].PutValue($"InActiveDepartment {c} - ItemId");
				ws.Cells[i, offset + count + 1].PutValue($"InActiveDepartment {c} - ItemCount");
				ws.Cells[i, offset + count + 2].PutValue($"InActiveDepartment {c} - ItemText");

				count += 3;
			}

			offset += 30;
			count = 0;

			for (var c = 0; c < 10; c++)
			{
				ws.Cells[i, offset + count].PutValue($"ForumThredResponders {c} - ItemId");
				ws.Cells[i, offset + count + 1].PutValue($"ForumThredResponders {c} - ItemCount");
				ws.Cells[i, offset + count + 2].PutValue($"ForumThredResponders {c} - ItemText");

				count += 3;
			}


			offset += 30;
			count = 0;

			for (var c = 0; c < 10; c++)
			{
				ws.Cells[i, offset + count].PutValue($"PagesViewed {c} - ItemId");
				ws.Cells[i, offset + count + 1].PutValue($"PagesViewed {c} - ItemCount");
				ws.Cells[i, offset + count + 2].PutValue($"PagesViewed {c} - ItemText");

				count += 3;
			}

			offset += 30;
			count = 0;

			for (var c = 0; c < 10; c++)
			{
				ws.Cells[i, offset + count].PutValue($"BlogsViewed {c} - ItemId");
				ws.Cells[i, offset + count + 1].PutValue($"BlogsViewed {c} - ItemCount");
				ws.Cells[i, offset + count + 2].PutValue($"BlogsViewed {c} - ItemText");

				count += 3;
			}

			i = 1;
			foreach (var agg in usage)
			{
				var key = (agg.PayLoad.TenantGuid + "_" + agg.PayLoad.LicenceKey).ToUpper();
				var tenant = "Unknown";

				if (!processedKeys.Contains(key))
				{
					if (tenants.ContainsKey(key))
					{
						tenant = tenants[key];
					}

					ws.Cells[i, 0].PutValue(tenant);
					ws.Cells[i, 1].PutValue(agg.PayLoad.TenantGuid);
					ws.Cells[i, 2].PutValue(agg.PayLoad.LicenceKey);
					ws.Cells[i, 3].PutValue(agg.PayLoad.LicencesPermitted);
					ws.Cells[i, 4].PutValue(agg.PayLoad.ActiveUserCount);
					ws.Cells[i, 5].PutValue(agg.PayLoad.InactiveUserCount);
					ws.Cells[i, 6].PutValue(agg.PayLoad.ContentAreaCount);
					ws.Cells[i, 7].PutValue(agg.PayLoad.BlogCount);
					ws.Cells[i, 8].PutValue(agg.PayLoad.TeamCount);
					ws.Cells[i, 9].PutValue(agg.PayLoad.HighWarningCount);
					ws.Cells[i, 10].PutValue(agg.PayLoad.MediumWarningCount);
					ws.Cells[i, 11].PutValue(agg.PayLoad.LowWarningCount);
					ws.Cells[i, 12].PutValue(agg.PayLoad.PagesAdded);
					ws.Cells[i, 13].PutValue(agg.PayLoad.BlogPostsAdded);
					ws.Cells[i, 14].PutValue(agg.PayLoad.ForumThreadsAdded);
					ws.Cells[i, 15].PutValue(agg.PayLoad.ForumPostsAdded);
					ws.Cells[i, 16].PutValue(agg.PayLoad.TimelineEntriesAdded);
					ws.Cells[i, 17].PutValue(agg.PayLoad.EndorsementsAdded);
					ws.Cells[i, 18].PutValue(agg.PayLoad.OptIn);
					ws.Cells[i, 19].PutValue(agg.PayLoad.Sector);
					ws.Cells[i, 20].PutValue(agg.PayLoad.UserRange);
					ws.Cells[i, 21].PutValue(agg.PayLoad.NumberOfContributors);
					ws.Cells[i, 22].PutValue(agg.PayLoad.NumberOfExpiredPages);
					ws.Cells[i, 23].PutValue(agg.PayLoad.NumberOfUnrenewedPages);
					ws.Cells[i, 24].PutValue(agg.PayLoad.MonthlyActiveUsersPrev);
					ws.Cells[i, 25].PutValue(agg.PayLoad.MonthlyActiveVisitsPrev);
					ws.Cells[i, 26].PutValue(agg.PayLoad.MonthlyActiveUsersPastFirstPagePrev);
					ws.Cells[i, 27].PutValue(agg.PayLoad.MonthlyVisitsUniquePastFirstPagePrev);
					ws.Cells[i, 28].PutValue(agg.PayLoad.MonthlyActiveUsersCurr);
					ws.Cells[i, 29].PutValue(agg.PayLoad.MonthlyActiveVisitsCurr);
					ws.Cells[i, 30].PutValue(agg.PayLoad.MonthlyActiveUsersPastFirstPageCurr);
					ws.Cells[i, 31].PutValue(agg.PayLoad.MonthlyVisitsUniquePastFirstPageCurr);
					ws.Cells[i, 32].PutValue(agg.PayLoad.OptInLastUpdatedBy);
					ws.Cells[i, 33].PutValue(agg.PayLoad.OptInlastUpdatedDate);
					ws.Cells[i, 34].PutValue(agg.PayLoad.Version);
					ws.Cells[i, 35].PutValue(agg.PayLoad.DomainName);

					offset = 36;
					count = 0;

					foreach (var tmp in agg.PayLoad.Top10ActiveDepartments)
					{
						ws.Cells[i, offset + count].PutValue(tmp.ItemId);
						ws.Cells[i, offset + count + 1].PutValue(tmp.ItemCount);
						ws.Cells[i, offset + count + 2].PutValue(tmp.ItemText);

						count += 3;
					}

					offset += 30;
					count = 0;

					foreach (var tmp in agg.PayLoad.Top10InactiveDepartments)
					{
						ws.Cells[i, offset + count].PutValue(tmp.ItemId);
						ws.Cells[i, offset + count + 1].PutValue(tmp.ItemCount);
						ws.Cells[i, offset + count + 2].PutValue(tmp.ItemText);

						count += 3;
					}

					offset += 30;
					count = 0;

					foreach (var tmp in agg.PayLoad.Top10ForumThreadResponders)
					{
						ws.Cells[i, offset + count].PutValue(tmp.ItemId);
						ws.Cells[i, offset + count + 1].PutValue(tmp.ItemCount);
						ws.Cells[i, offset + count + 2].PutValue(tmp.ItemText);

						count += 3;
					}

					offset += 30;
					count = 0;

					foreach (var tmp in agg.PayLoad.Top10PagesViewed)
					{
						ws.Cells[i, offset + count].PutValue(tmp.ItemId);
						ws.Cells[i, offset + count + 1].PutValue(tmp.ItemCount);
						ws.Cells[i, offset + count + 2].PutValue(tmp.ItemText);

						count += 3;
					}

					offset += 30;
					count = 0;

					foreach (var tmp in agg.PayLoad.Top10BlogsViewed)
					{
						ws.Cells[i, offset + count].PutValue(tmp.ItemId);
						ws.Cells[i, offset + count + 1].PutValue(tmp.ItemCount);
						ws.Cells[i, offset + count + 2].PutValue(tmp.ItemText);

						count += 3;
					}
					
					i++;

					processedKeys.Add(key);
				}
			}
		}
	}

	class modData
	{
		public int AbsenceBooking = 0;
		public int Administration = 0;
		public int Analytics = 0;
		public int Blog = 0;
		public int BusinessContacts = 0;
		public int Calendar = 0;
	    public int CaseManagement = 0;
		public int Category = 0;
		public int ContentAreaTeamHomepageHomepage = 0;
	    public int DeveloperPlatform = 0;
		public int ErrorPage = 0;
		public int Expenses = 0;
		public int Football = 0;
		public int Forums = 0;
		public int Gallery = 0;
	    public int GettingStarted = 0;
	    public int HomePage = 0;
	    public int ManagerHomepagesAdmin = 0;
	    public int ManageIpAddresses = 0;
		public int NetworkDirectory = 0;
		public int Notifications = 0;
		public int Organogram = 0;
		public int Other = 0;
		public int Page = 0;
		public int PageEditor = 0;
		public int PeopleDirectory = 0;
	    public int Polls = 0;
		public int RoomResource = 0;
		public int Search = 0;
	    public int SharepointAdmin = 0;
	    public int ShortUrlAdmin = 0;
	    public int SystemTextAdmin = 0;
		public int Tasks = 0;
		public int TeamBlog = 0;
		public int Teams = 0;
	    public int TrainingManager = 0;
	    public int Umi = 0;
		public int WorkflowForms = 0;
	}
}
