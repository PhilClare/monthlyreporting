﻿using Aspose.Cells;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Pod.Telemetry.Reporting.Models;

using Simple.OData.Client;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Http.Extensions.Compression.Client;
using System.Net.Http.Extensions.Compression.Core.Compressors;
using System.Web;

namespace Reports.Sources
{
	class Zendesk
	{
		public static async Task GetServicedeskData(string tenantOrganisation, DateTime startDate, DateTime endDate, Worksheet ws)
		{
			endDate = endDate.AddDays(-1);
			var orgSearch = $"organization:{tenantOrganisation}";
			var solvedSearch = $"solved>={startDate.ToString("yyyy-MM-dd")} solved<={endDate.ToString("yyyy-MM-dd")}";
			var createdSearch = $"created>={startDate.ToString("yyyy-MM-dd")} created<={endDate.ToString("yyyy-MM-dd")}";


			// get tickets for opened / solved
			var opened = await DoSearch($"{orgSearch} {createdSearch}", "created_at");
			var solved = await DoSearch($"{orgSearch} {solvedSearch}", "solved");
			
			ws.Cells[1, 6].PutValue(solved.counts.low);
			ws.Cells[2, 6].PutValue(solved.counts.medium);
			ws.Cells[3, 6].PutValue(solved.counts.high);
			ws.Cells[4, 6].PutValue(solved.counts.urgent);
			ws.Cells[6, 6].PutValue(opened.count);
			ws.Cells[7, 6].PutValue(solved.count);
			ws.Cells[9, 6].PutValue(solved.counts.bad);
			ws.Cells[10, 6].PutValue(solved.counts.good);
			ws.Cells[11, 6].PutValue(solved.counts.offered);

			ws.Cells[13, 6].PutValue(opened.counts.incident);
			ws.Cells[14, 6].PutValue(opened.counts.task);
			ws.Cells[15, 6].PutValue(opened.counts.question);
			ws.Cells[16, 6].PutValue(opened.counts.problem);

			var i = 0;
			var offset = 20;
			foreach(var result in opened.results)
			{
				ws.Cells[i + offset, 8].PutValue(result.subject);
				ws.Cells[i + offset, 9].PutValue(result.created_at);
				ws.Cells[i + offset, 10].PutValue(result.priority);
				ws.Cells[i + offset, 11].PutValue(result.priority);
				i++;

				if(i > 5)
				{
					break;
				}
			}

			i = 0;
			offset = 28;
			foreach (var result in solved.results)
			{
				ws.Cells[i + offset, 8].PutValue(result.subject);
				ws.Cells[i + offset, 9].PutValue(result.created_at);
				ws.Cells[i + offset, 10].PutValue(result.priority);
				ws.Cells[i + offset, 11].PutValue(result.priority);

				i++;

				if (i > 5)
				{
					break;
				}
			}

		}

		private static async Task<QueryResult> DoSearch(string searchTerm, string orderBy)
		{
			var username = "phil.clare@interact-intranet.com/token";
			var password = "UakQUEyLakOdlVR74UvWQcSL5fQeU9CQgN1kP43x";
			var zdUrl = "https://interactintranet1.zendesk.com";
			var searchUrl = $"{zdUrl}/api/v2/search.json?query={searchTerm}&sort_by={orderBy}&sort_order=desc";
			var count = 0;

			var req = (HttpWebRequest)WebRequest.Create(searchUrl);
			var auth = Convert.ToBase64String(Encoding.ASCII.GetBytes($"{username}:{password}"));
			req.Headers.Add("Authorization", $"Basic {auth}");
			req.PreAuthenticate = true;
			req.Method = "GET";
			req.ContentType = "application/json";
			req.Accept = "application/json";

			var res = req.GetResponse();
			var response = res as HttpWebResponse;
			string responseFromServer = string.Empty;
			using (var responseStream = response.GetResponseStream())
			{
				using (var reader = new StreamReader(responseStream))
				{
					responseFromServer = reader.ReadToEnd();
				}
			}

			QueryResult obj = JsonConvert.DeserializeObject<QueryResult>(responseFromServer);

			return obj;
		}

		public class Counts
		{
			public int low { get; set; }
			public int medium { get; set; }
			public int high { get; set; }
			public int urgent { get; set; }

			public int bad { get; set; }
			public int good { get; set; }
			public int offered { get; set; }

			public int task { get; set; }
			public int incident { get; set; }
			public int question { get; set; }
			public int problem { get; set; }
		}

		public class QueryResult
		{
			public Result[] results { get; set; }
			public object facets { get; set; }
			public object next_page { get; set; }
			public object previous_page { get; set; }
			public int count { get; set; }

			private Counts _counts;

			public Counts counts {
				get
				{
					if(_counts == null)
					{
						_counts = new Counts();

						foreach(Result res in this.results)
						{
							switch (res.priority)
							{
								case "low":
									_counts.low += 1;
									break;
								case "medium":
									_counts.medium += 1;
									break;
								case "high":
									_counts.high += 1;
									break;
								case "urgent":
									_counts.urgent += 1;
									break;
							}

							switch (res.satisfaction_rating.score)
							{
								case "good":
									_counts.good += 1;
									break;
								case "offered":
									_counts.offered += 1;
									break;
								case "bad":
									_counts.bad += 1;
									break;
							}

							switch (res.type)
							{
								case "incident":
									_counts.incident += 1;
									break;
								case "task":
									_counts.task += 1;
									break;
								case "question":
									_counts.question += 1;
									break;
								case "problem":
									_counts.problem += 1;
									break;
							}
						}
					}

					return _counts;
				}
			}
		}

		public class Result
		{
			public string url { get; set; }
			public int id { get; set; }
			public object external_id { get; set; }
			public Via via { get; set; }
			public DateTime created_at { get; set; }
			public DateTime updated_at { get; set; }
			public string type { get; set; }
			public string subject { get; set; }
			public string raw_subject { get; set; }
			public string description { get; set; }
			public string priority { get; set; }
			public string status { get; set; }
			public string recipient { get; set; }
			public long requester_id { get; set; }
			public long submitter_id { get; set; }
			public long assignee_id { get; set; }
			public long organization_id { get; set; }
			public int group_id { get; set; }
			public long?[] collaborator_ids { get; set; }
			public object forum_topic_id { get; set; }
			public object problem_id { get; set; }
			public bool has_incidents { get; set; }
			public bool is_public { get; set; }
			public object due_at { get; set; }
			public string[] tags { get; set; }
			public Custom_Fields[] custom_fields { get; set; }
			public Satisfaction_Rating satisfaction_rating { get; set; }
			public object[] sharing_agreement_ids { get; set; }
			public Field[] fields { get; set; }
			public object[] followup_ids { get; set; }
			public int? ticket_form_id { get; set; }
			public int brand_id { get; set; }
			public float? satisfaction_probability { get; set; }
			public bool allow_channelback { get; set; }
			public string result_type { get; set; }
		}

		public class Via
		{
			public string channel { get; set; }
			public Source source { get; set; }
		}

		public class Source
		{
			public From from { get; set; }
			public To to { get; set; }
			public object rel { get; set; }
		}

		public class From
		{
			public string address { get; set; }
			public string name { get; set; }
		}

		public class To
		{
			public string name { get; set; }
			public string address { get; set; }
		}

		public class Satisfaction_Rating
		{
			public string score { get; set; }
			public int id { get; set; }
			public object comment { get; set; }
			public string reason { get; set; }
			public int reason_id { get; set; }
		}

		public class Custom_Fields
		{
			public int id { get; set; }
			public string value { get; set; }
		}

		public class Field
		{
			public int id { get; set; }
			public string value { get; set; }
		}

	}

}
