﻿using Aspose.Cells;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Pod.Telemetry.Reporting.Models;
using Pod.Core.Models;
using Simple.OData.Client;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Http.Extensions.Compression.Client;
using System.Net.Http.Extensions.Compression.Core.Compressors;

namespace Reports.Sources
{
	class Telemetry
	{
		public static async Task GetTelemetry(Guid tenantGuid, DateTime startDate, DateTime endDate, Worksheet ws, string username, string password, string reportsUrl)
		{
			var start = (DateTimeOffset?)startDate;
			var end = (DateTimeOffset?)endDate;

			// Create client with given base URI and credentials and enable gzip
			var settings = new ODataClientSettings
			{
				BaseUri = new Uri($"{reportsUrl}/odata"),
				OnCreateMessageHandler =
					() =>
						new ClientCompressionHandler(new GZipCompressor()),
				BeforeRequest = request =>
				{
					request.Headers.Add("Accept-Encoding", "gzip");
					request.Headers.Authorization = new AuthenticationHeaderValue(
						"Basic",
						Convert.ToBase64String(
							Encoding.ASCII.GetBytes(
								$"{username}:{password}")));
				}
			};

			var client = new ODataClient(settings);

			var usagePerDay = await GetUsage(client, tenantGuid, startDate, endDate);

			var currentDate = startDate;
			int row = 1;

			// clear existing data
			while (row <= 31)
			{
				ws.Cells[row, 0].PutValue(null);
				ws.Cells[row, 1].PutValue(null);
				ws.Cells[row, 2].PutValue(null);
				ws.Cells[row, 3].PutValue(null);

				row++;
			}

			row = 1;
			while (currentDate < endDate)
			{
				ws.Cells[row, 0].PutValue(currentDate);
				ws.Cells[row, 1].PutValue(usagePerDay[currentDate].UniqueVisitors);
				ws.Cells[row, 2].PutValue(usagePerDay[currentDate].Sessions);
				ws.Cells[row, 3].PutValue(usagePerDay[currentDate].PageHits);

				row++;
				currentDate = currentDate.AddDays(1);
			}

			// day by day average visitors
			Dictionary<int, int> weekDays = new Dictionary<int, int>();
			Dictionary<int, int> occurences = new Dictionary<int, int>();
			for (var i = 0; i <= 6; i++)
			{
				weekDays.Add(i, 0);
				occurences.Add(i, 0);
			}

			currentDate = startDate;
			while (currentDate < endDate)
			{
				int dow = (int)currentDate.DayOfWeek;

				weekDays[dow] += usagePerDay[currentDate].UniqueVisitors;
				occurences[dow] += 1;

				currentDate = currentDate.AddDays(1);
			}

			for (var i = 0; i <= 6; i++)
			{
			    if (occurences[i] > 0)
			    {
			        weekDays[i] = weekDays[i] / occurences[i];
			    }
			}

			row = 0;
			var offset = 43;
			while (row <= 6)
			{
				ws.Cells[row + offset, 1].PutValue(null);

				row++;
			}

			ws.Cells[43, 1].PutValue(weekDays[0]);
			ws.Cells[44, 1].PutValue(weekDays[6]);
			ws.Cells[45, 1].PutValue(weekDays[5]);
			ws.Cells[46, 1].PutValue(weekDays[4]);
			ws.Cells[47, 1].PutValue(weekDays[3]);
			ws.Cells[48, 1].PutValue(weekDays[2]);
			ws.Cells[49, 1].PutValue(weekDays[1]);
			

			// Get monthly usage
			var monthlySDate = startDate.AddMonths(-6);
			monthlySDate = monthlySDate.AddDays(1 - monthlySDate.Day);

			ws.Cells[20, 6].PutValue(monthlySDate.ToString("MMMM yyyy"));

			var usagePerMonth = await GetMonthlyUsage(client, tenantGuid, monthlySDate, endDate);

			currentDate = startDate.AddMonths(-6);
			row = 1;
			offset = 33;
			// clear existing data
			while (row <= 6)
			{
				ws.Cells[row + offset, 0].PutValue(null);
				ws.Cells[row + offset, 1].PutValue(null);

				row++;
			}

			row = 1;
			while (row <= 6)
			{
				ws.Cells[row + offset, 0].PutValue(currentDate);
				ws.Cells[row + offset, 1].PutValue(usagePerMonth[currentDate].UniqueVisitors);

				row++;
				currentDate = currentDate.AddMonths(1);
			}

			// Get Aggregate
			var aggregate = await GetAggregate(client, tenantGuid, startDate, endDate);

			ws.Cells[1, 9].PutValue(aggregate.PayLoad.ActiveUserCount);
			ws.Cells[2, 9].PutValue(aggregate.PayLoad.ContentAreaCount);
			ws.Cells[3, 9].PutValue(aggregate.PayLoad.TeamCount);
			ws.Cells[4, 9].PutValue(aggregate.PayLoad.HighWarningCount);
			ws.Cells[5, 9].PutValue(aggregate.PayLoad.MediumWarningCount);
			ws.Cells[6, 9].PutValue(aggregate.PayLoad.LowWarningCount);
			ws.Cells[7, 9].PutValue(aggregate.PayLoad.PagesAdded);
			ws.Cells[8, 9].PutValue(aggregate.PayLoad.BlogPostsAdded);
			ws.Cells[9, 9].PutValue(aggregate.PayLoad.ForumThreadsAdded);
			ws.Cells[10, 9].PutValue(aggregate.PayLoad.ForumPostsAdded);
			ws.Cells[11, 9].PutValue(aggregate.PayLoad.TimelineEntriesAdded);
			ws.Cells[12, 9].PutValue(aggregate.PayLoad.EndorsementsAdded);
			ws.Cells[13, 9].PutValue(aggregate.PayLoad.NumberOfExpiredPages);
			ws.Cells[14, 9].PutValue(aggregate.PayLoad.NumberOfContributors);


			ws.Cells[1, 16].PutValue(aggregate.PayLoad.MonthlyActiveUsersCurr);
			ws.Cells[1, 17].PutValue(aggregate.PayLoad.MonthlyActiveUsersPrev);
			ws.Cells[2, 16].PutValue(aggregate.PayLoad.MonthlyActiveVisitsCurr);
			ws.Cells[2, 17].PutValue(aggregate.PayLoad.MonthlyActiveVisitsPrev);
			ws.Cells[3, 16].PutValue(aggregate.PayLoad.MonthlyActiveUsersPastFirstPageCurr);
			ws.Cells[3, 17].PutValue(aggregate.PayLoad.MonthlyActiveUsersPastFirstPagePrev);
			ws.Cells[4, 16].PutValue(aggregate.PayLoad.MonthlyVisitsUniquePastFirstPageCurr);
			ws.Cells[4, 17].PutValue(aggregate.PayLoad.MonthlyVisitsUniquePastFirstPagePrev);

			// Top 10 stuff
			offset = 36;
			for (var i = 0; i <= 9; i++)
			{
				ws.Cells[offset + i, 8].PutValue(" ");
				ws.Cells[offset + i, 9].PutValue(" ");
				ws.Cells[offset + i, 10].PutValue(" ");
				ws.Cells[offset + i, 11].PutValue(" ");
				ws.Cells[offset + i, 12].PutValue(" ");
				ws.Cells[offset + i, 13].PutValue(" ");
				ws.Cells[offset + i, 14].PutValue(" ");
				ws.Cells[offset + i, 15].PutValue(" ");
				ws.Cells[offset + i, 16].PutValue(" ");
				ws.Cells[offset + i, 17].PutValue(" ");


				if (aggregate.PayLoad.Top10ActiveDepartments.Length > i)
				{
					ws.Cells[offset + i, 8].PutValue(aggregate.PayLoad.Top10ActiveDepartments[i].ItemText);
					ws.Cells[offset + i, 9].PutValue(aggregate.PayLoad.Top10ActiveDepartments[i].ItemCount);
				}
				if (aggregate.PayLoad.Top10InactiveDepartments.Length > i)
				{
					ws.Cells[offset + i, 10].PutValue(aggregate.PayLoad.Top10InactiveDepartments[i].ItemText);
					ws.Cells[offset + i, 11].PutValue(aggregate.PayLoad.Top10InactiveDepartments[i].ItemCount);
				}
				if (aggregate.PayLoad.Top10ForumThreadResponders.Length > i)
				{
					ws.Cells[offset + i, 12].PutValue(aggregate.PayLoad.Top10ForumThreadResponders[i].ItemText);
					ws.Cells[offset + i, 13].PutValue(aggregate.PayLoad.Top10ForumThreadResponders[i].ItemCount);
				}
				if (aggregate.PayLoad.Top10PagesViewed.Length > i)
				{
					ws.Cells[offset + i, 14].PutValue(aggregate.PayLoad.Top10PagesViewed[i].ItemText);
					ws.Cells[offset + i, 15].PutValue(aggregate.PayLoad.Top10PagesViewed[i].ItemCount);
				}
				if (aggregate.PayLoad.Top10BlogsViewed.Length > i)
				{
					ws.Cells[offset + i, 16].PutValue(aggregate.PayLoad.Top10BlogsViewed[i].ItemText);
					ws.Cells[offset + i, 17].PutValue(aggregate.PayLoad.Top10BlogsViewed[i].ItemCount);
				}
			}
			if (!aggregate.PayLoad.OptIn)
			{
				ws.Workbook.Worksheets[3].SetVisible(false, true);
			}
			
			// Get busiest times
			var periods = await GetPeriods(client, tenantGuid, startDate, endDate);

			row = 22;
			foreach (var period in periods)
			{
				var dow = Enum.GetName(typeof(DayOfWeek), int.Parse(period.Key.Split('-')[0]));
				var hour = int.Parse(period.Key.Split('-')[1]);
				ws.Cells[row, 6].PutValue($"{dow} at {hour}:00");

				row++;
			}

			// Get browser
			var browsers = await GetBrowsers(client, tenantGuid, startDate, endDate);

			row = 53;
			foreach (var period in browsers)
			{
				ws.Cells[row, 1].PutValue(period.Value.Mobile);
				ws.Cells[row, 2].PutValue(period.Value.Desktop);

				row++;
			}

			// Get modules
			var modules = await GetModules(client, tenantGuid, startDate, endDate);

			row = 53;
			foreach (var module in modules)
			{
				ws.Cells[row, 5].PutValue(module.Key);
				ws.Cells[row, 6].PutValue(module.Value);

				row++;
			}

			ws.CalculateFormula(true, true, null);
		}

		private static async Task<Dictionary<DateTime, dateDetails>> GetUsage(ODataClient client, Guid tenantGuid, DateTime startDate, DateTime endDate)
		{
			var start = (DateTimeOffset?)startDate;
			var end = (DateTimeOffset?)endDate.AddDays(1);

			var annotations = new ODataFeedAnnotations();

			var usage = (await client
				.For<Traffic>()
				.Filter(p => p.TenantGuid == tenantGuid &&
							 p.TrafficStartDate >= start &&
							 p.TrafficEndDate <= end)
				.Select(p => new { p.TrafficStartDate, p.UniqueVisitors, p.PageHits, p.Sessions })
				.FindEntriesAsync(annotations))
				.ToList();

			while (annotations.NextPageLink != null)
			{
				usage.AddRange(await client
				.For<Traffic>()
				.Filter(p => p.TenantGuid == tenantGuid &&
							 p.TrafficStartDate >= start &&
							 p.TrafficEndDate <= end)
				.Select(p => new { p.TrafficStartDate, p.UniqueVisitors, p.PageHits, p.Sessions })
				.FindEntriesAsync(annotations.NextPageLink, annotations));
			}

			var usagePerDay = new Dictionary<DateTime, dateDetails>();
			var currentDate = startDate;

			while (currentDate <= endDate)
			{
				usagePerDay.Add(currentDate, new dateDetails() { Date = currentDate, UniqueVisitors = 0, Sessions = 0, PageHits = 0 });
				currentDate = currentDate.AddDays(1);
			}

			foreach (var dataPoint in usage)
			{
				if (dataPoint.TrafficStartDate.HasValue)
				{
					var details = new dateDetails() { Date = dataPoint.TrafficStartDate.Value, UniqueVisitors = dataPoint.UniqueVisitors.Value, Sessions = dataPoint.Sessions.Value, PageHits = dataPoint.PageHits.Value };
					usagePerDay[dataPoint.TrafficStartDate.Value] = details;
				}
			}

			return usagePerDay;

		}

		private static async Task<Dictionary<DateTime, dateDetails>> GetMonthlyUsage(ODataClient client, Guid tenantGuid, DateTime startDate, DateTime endDate)
		{
			var start = (DateTimeOffset?)startDate;
			var end = (DateTimeOffset?)endDate;

			var annotations = new ODataFeedAnnotations();

			var currentDate = startDate;
			List<MonthlyTraffic> usage = new List<MonthlyTraffic>();
			var usagePerMonth = new Dictionary<DateTime, dateDetails>();

			while (currentDate < endDate)
			{
				int mon = currentDate.Month;
				int yr = currentDate.Year;
				var dt = new DateTime(currentDate.Year, currentDate.Month, 1);

				usagePerMonth.Add(currentDate, new dateDetails() { Date = dt, UniqueVisitors = 0, Sessions = 0, PageHits = 0 });

				usage = (await client
					.For<MonthlyTraffic>()
					.Filter(p => p.TenantGuid == tenantGuid &&
								 p.TrafficMonth == mon &&
								 p.TrafficYear == yr)
					.FindEntriesAsync(annotations))
					.ToList();

				while (annotations.NextPageLink != null)
				{
					usage.AddRange(await client
					.For<MonthlyTraffic>()
					.Filter(p => p.TenantGuid == tenantGuid &&
								 p.TrafficMonth == mon &&
								 p.TrafficYear == yr)
					.FindEntriesAsync(annotations.NextPageLink, annotations));
				}

				if (usage.Count > 0)
				{
					var details = new dateDetails() { Date = dt, UniqueVisitors = usage[0].UniqueVisitors.Value, Sessions = usage[0].Sessions.Value, PageHits = usage[0].PageHits.Value };
					usagePerMonth[dt] = details;
				}

				currentDate = currentDate.AddMonths(1);
			}

			return usagePerMonth;
		}

		private static async Task<AggregateDataResponse> GetAggregate(ODataClient client, Guid tenantGuid, DateTime startDate, DateTime endDate)
		{
			var start = (DateTimeOffset?)startDate;
			var end = (DateTimeOffset?)endDate;

			var annotations = new ODataFeedAnnotations();

			var usage = (await client
				.For<AggregateDataResponse>()
				.Filter(p => p.PayLoad.TenantGuid == tenantGuid &&
							 p.InsertionDate >= start &&
							 p.InsertionDate <= end)
				.FindEntriesAsync(annotations))
				.ToList();

			while (annotations.NextPageLink != null)
			{
				usage.AddRange(await client
				.For<AggregateDataResponse>()
				.Filter(p => p.PayLoad.TenantGuid == tenantGuid &&
							 p.InsertionDate >= start &&
							 p.InsertionDate <= end)
				.FindEntriesAsync(annotations.NextPageLink, annotations));
			}

			var aggregate = new aggregateData();
			
			if (usage.Count > 0)
			{
				var last = usage.Count - 1;

				return usage[last];
			}

			return null;

		}

		private static async Task<Dictionary<string, int>> GetPeriods(ODataClient client, Guid tenantGuid, DateTime startDate, DateTime endDate)
		{
			var start = (DateTimeOffset?)startDate;
			var end = (DateTimeOffset?)endDate;

			var annotations = new ODataFeedAnnotations();

			var usage = (await client
				.For<HourlyTraffic>()
				.Filter(p => p.TenantGuid == tenantGuid &&
							 p.UpdatedDate >= start &&
							 p.UpdatedDate <= end)
				.FindEntriesAsync(annotations))
				.ToList();

			while (annotations.NextPageLink != null)
			{
				usage.AddRange(await client
				.For<HourlyTraffic>()
				.Filter(p => p.TenantGuid == tenantGuid &&
							 p.UpdatedDate >= start &&
							 p.UpdatedDate <= end)
				.FindEntriesAsync(annotations.NextPageLink, annotations));
			}

			var usagePerDayHour = new Dictionary<string, int>();

			foreach (var dataPoint in usage)
			{
				if (dataPoint.UpdatedDate.HasValue)
				{
					var key = $"{dataPoint.TrafficDay}-{dataPoint.TrafficHour}";

					if (usagePerDayHour.ContainsKey(key))
					{
						usagePerDayHour[key] = dataPoint.Sessions.Value;
					}
					else
					{
						usagePerDayHour.Add(key, dataPoint.Sessions.Value);
					}
				}
			}

			var sortedDict = (from entry in usagePerDayHour orderby entry.Value descending select entry).Take(3);

			return sortedDict.ToDictionary(t => t.Key, t => t.Value);

		}

		private static async Task<Dictionary<string, int>> GetModules(ODataClient client, Guid tenantGuid, DateTime startDate, DateTime endDate)
		{
			var start = (DateTimeOffset?)startDate;
			var end = (DateTimeOffset?)endDate;

			var annotations = new ODataFeedAnnotations();

			var usage = (await client
				.For<Page>()
				.Filter(p => p.TenantGuid == tenantGuid &&
							 p.TrafficStartDate >= start &&
							 p.TrafficEndDate <= end)
				.FindEntriesAsync(annotations))
				.ToList();

			while (annotations.NextPageLink != null)
			{
				usage.AddRange(await client
				.For<Page>()
				.Filter(p => p.TenantGuid == tenantGuid &&
							 p.TrafficStartDate >= start &&
							 p.TrafficEndDate <= end)
				.FindEntriesAsync(annotations.NextPageLink, annotations));
			}

			var usagePerDayHour = new Dictionary<string, int>();

			foreach (var dataPoint in usage)
			{
				if (dataPoint.Module != null && dataPoint.Module.ToLower() != "admin" && dataPoint.Module.ToLower() != "error" && dataPoint.Total.HasValue)
				{
					var key = dataPoint.Module;

					if (usagePerDayHour.ContainsKey(key))
					{
						usagePerDayHour[key] = dataPoint.Total.Value;
					}
					else
					{
						usagePerDayHour.Add(key, dataPoint.Total.Value);
					}
				}
			}

			var sortedDict = (from entry in usagePerDayHour orderby entry.Value descending select entry).Take(5);

			return sortedDict.ToDictionary(t => t.Key, t => t.Value);

		}

		private static async Task<Dictionary<int, browserDetails>> GetBrowsers(ODataClient client, Guid tenantGuid, DateTime startDate, DateTime endDate)
		{
			var start = (DateTimeOffset?)startDate;
			var end = (DateTimeOffset?)endDate;

			var annotations = new ODataFeedAnnotations();

			var usage = (await client
				.For<HourlyBrowser>()
				.Filter(p => p.TenantGuid == tenantGuid &&
							 p.UpdatedDate >= start &&
							 p.UpdatedDate <= end)
				.FindEntriesAsync(annotations))
				.ToList();

			while (annotations.NextPageLink != null)
			{
				usage.AddRange(await client
				.For<HourlyBrowser>()
				.Filter(p => p.TenantGuid == tenantGuid &&
							 p.UpdatedDate >= start &&
							 p.UpdatedDate <= end)
				.FindEntriesAsync(annotations.NextPageLink, annotations));
			}

			var usagePerDayHour = new Dictionary<int, browserDetails>();

			for (var i = 0; i <= 23; i++)
			{
				var browser = new browserDetails() { Desktop = 0, Mobile = 0 };
				usagePerDayHour[i] = browser;
			}

			foreach (var dataPoint in usage)
			{
				if (dataPoint.UpdatedDate.HasValue)
				{
					var key = dataPoint.TrafficHour.Value;
					var bd = usagePerDayHour[key];

					switch (dataPoint.DeviceType.ToLower())
					{
						case "mobile":
							bd.Mobile += dataPoint.Sessions.Value;
							break;
						case "desktop":
							bd.Desktop += dataPoint.Sessions.Value;
							break;
					}
					usagePerDayHour[key] = bd;
				}
			}

			return usagePerDayHour;
		}
	}

	struct browserDetails
	{
		public int Desktop;
		public int Mobile;
	}

	struct dateDetails
	{
		public DateTime Date;
		public int UniqueVisitors;
		public int Sessions;
		public int PageHits;
	}

	struct aggregateData
	{
		public int ActiveUsers;
		public int ContentAreas;
		public int Teams;
		public int HighWarnings;
		public int MediumWarnings;
		public int LowWarnings;
		public int Pages;
		public int BlogPosts;
		public int ForumThreads;
		public int ForumPosts;
		public int TimelineEntries;
		public int Endorsements;
	}
}
